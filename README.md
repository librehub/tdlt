# tdlt 

Simple cli project todo manager.

## Usage

**Add task**
```sh
$ tdlt add "TASK TITLE" TASK_PROGRESS
$ tdlt add "My title" 0. 
```

**Remove task**
```sh
$ tdlt remove TASK_ID
$ tdlt remove 1 
```

**Set task progress**
```sh
$ tdlt progress TASK_ID TASK_PROGRESS
$ tdlt progress 1 0.5
```

**View tasks**
```sh
$ tdlt view
```

## Build and install

```sh
$ git clone https://codeberg.org/librehub/tdlt.git
$ cd tdlt/bin
$ sh build.sh
$ sudo cp ./tdlt /usr/bin
```
## Contacts

| Contact                                               | Description       |
| :---:                                                 | :---              |
| [`Matrix`](https://matrix.to/#/#librehub:matrix.org)  | Matrix server.    |
| [`Discord`](https://discord.gg/naGkzRN)               | Discord server.   |

## Donates
**Monero:** `47KkgEb3agJJjSpeW1LpVi1M8fsCfREhnBCb1yib5KQgCxwb6j47XBQAamueByrLUceRinJqveZ82UCbrGqrsY9oNuZ97xN`

