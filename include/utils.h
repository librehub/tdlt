
#ifndef UTILS_H
#define UTILS_H

#include <fstream>
#include <string>

using namespace std;

inline bool writeTextToFile(string path, string text) {
    ofstream file(path);
    if (file.is_open()) {
        file << text;
        file.close();
        return true;
    }
    return false;
}


#endif

