
#ifndef TDLT_H
#define TDLT_H

#include <string>

#include "task.h"

using namespace std;

class Tdlt {

public:
    string filepath = "";

    Tdlt(string filepath);

    void incTasksCount();
    int getTasksCount();

    int addTask(string title, float progress);
    bool removeTask(int id); 
    bool setProgress(int id, float progress);

    json parseFile();

    void view();
    void viewCategory(string category, int color, vector<Task> items);
};

#endif

