CXXFLAGS ?= -Wall -g
CXXFLAGS += -std=c++11

all: tdlt

HEADERS = \
	include/task.h \
	include/tdlt.h \
	include/colors.h \
	include/utils.h \
	libs/args.hxx
SOURCES = \
    src/tdlt.cpp \
    src/main.cpp
OBJECTS = $(SOURCES:.cpp=.o)

tdlt: $(HEADERS) $(OBJECTS)
	$(CXX) -o $@ $(OBJECTS) $(LDFLAGS)

.PHONY: clean
clean:
	rm -f tdlt $(OBJECTS)

