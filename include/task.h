
#ifndef TASK_H
#define TASK_H

#include "utils.h"
#include "colors.h"

#include <nlohmann/json.hpp>
#include <iostream>
#include <fstream>
#include <string>

using namespace nlohmann;
using namespace std;

class Task {

public:
    int id = 0;
    string title = "";
    float progress = 0.f;

    Task(int id, string title, float progress) {
        this->id = id;
        this->title = title;
        this->progress = progress;
    }

    void view() {
        string percent = to_string((int)(progress * 100));
        cout << START_BOLD << GREEN << "m" << id << END << " -> "
             << title
             << START << BLUE << "m (" << percent << "%)" << END << endl;
    }

    bool write(string filepath) {
        ifstream file(filepath);
        json data = json::parse(file);

        data["items"].push_back(
            {
              {"id", id},
              {"title", title},
              {"progress", progress}
            }
        );

        string jsonData = data.dump(4);
        writeTextToFile(filepath, jsonData);
        return true;
    }

};

#endif

