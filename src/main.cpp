
#include "../include/tdlt.h"
#include "../libs/args.hxx"

#include <vector>
#include <string>

using namespace std;

int parseArgs(int argc, char *argv[], string &method, vector<string> &args) {
    args::ArgumentParser parser("tdlt - your cli project todo manager", "Report bugs to: https://codeberg.org/librehub/tdlt/issues");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});

    args::Group commands(parser, "commands");
    args::Command add(commands, "add", "add new task");
    args::Command remove(commands, "remove", "remove task by id");
    args::Command progress(commands, "progress", "set progress percentage of task");
    args::Command view(commands, "view", "view tasks");

    args::Group arguments(parser, "arguments", args::Group::Validators::DontCare, args::Options::Global);
    args::PositionalList<std::string> pathsList(arguments, "data", "data of task");

    try {
        parser.ParseCLI(argc, argv);
        if (add) {
            method = "add";
        } else if (remove) {
            method = "remove";
        } else if (progress) {
            method = "progress";
        } else if (view) {
            method = "view";
        }

        for (auto &&path : pathsList) {
            args.push_back(path);
        }
    }
    catch (args::Help) {
        std::cout << parser;
        return 0;
    }
    catch (args::ParseError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    catch (args::ValidationError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    return 0;
}

int main(int argc, char *argv[]) {
    string method = "";
    vector<string> args;

    int result = parseArgs(argc, argv, method, args);
    if (result > 0) {
        return result;
    }

    Tdlt tdlt = Tdlt(".tdlt");

    if (method == "add") {
        if (args.size() == 1) {
            tdlt.addTask(args[0], 0.f);
        } else if (args.size() > 1) {
            tdlt.addTask(args[0], atof(args[1].c_str()));
        } else {
            printf("ERROR: Task data does not provide!\n");
            return 1;
        }
    } else if (method == "remove") {
        if (args.size() > 0) { 
            tdlt.removeTask(atoi(args[0].c_str()));
        } else {
            printf("ERROR: Task id does not provide!\n");
            return 1;
        }
    } else if (method == "progress") {
        if (args.size() > 1) {
            tdlt.setProgress(atoi(args[0].c_str()), atof(args[1].c_str())); 
        } else {
            printf("ERROR: Task data does not provide!\n");
            return 1;
        }
    } else if (method == "view") {
        tdlt.view();
    }

    return 0;
}


