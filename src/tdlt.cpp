#include "../include/tdlt.h"
#include "../include/colors.h"
#include "../include/utils.h"

#include <nlohmann/json.hpp>

Tdlt::Tdlt(string filepath) {
    this->filepath = filepath;

    ifstream file(filepath);

    if (!file.is_open()) {
        json data = {
            {"items", {}},
            {"count", 0}
        };
        string text = data.dump(4);
        writeTextToFile(filepath, text);
    }
}

json Tdlt::parseFile() {
    ifstream f(filepath);
    return json::parse(f);
}

int Tdlt::getTasksCount() {
    json data = parseFile();

    return data["count"];
}

void Tdlt::incTasksCount() {
    json data = parseFile();

    int count = data["count"];
    data["count"] = count + 1;

    string text = data.dump(4);
    writeTextToFile(filepath, text);
}

int Tdlt::addTask(string title, float progress) {
    incTasksCount();
    int id = getTasksCount();
    Task task = Task(id, title, progress); 

    task.write(this->filepath);

    return id;
}

bool Tdlt::removeTask(int id) {
    json data = parseFile();

    int index = 0;
    for (auto &item : data["items"]) {
        if (item["id"] == id) { 
            break;
        }
        index++;
    }

    string title = data["items"][index]["title"];
    data["items"].erase(index);

    cout << START << RED << "m"
         << "Remove " << END
         << id
         << " (" << title << ")" << endl; 

    string text = data.dump(4);
    writeTextToFile(filepath, text);

    return true;
}

bool Tdlt::setProgress(int id, float progress) {
    json data = parseFile();

    for (auto &item : data["items"]) {
        if (item["id"] == id) { 
            item["progress"] = progress;
            break;
        }
    }

    string percent = to_string((int)(progress * 100));
    cout << START << YELLOW << "m"
         << "Set " << END
         << id
         << " progress to "
         << START << BLUE << "m" << percent << "%" << END << endl; 

    string text = data.dump(4);
    writeTextToFile(filepath, text);

    return true;
}

void Tdlt::viewCategory(string category, int color, vector<Task> items) {
    cout << START_BOLD << color << "m" << category << END << endl;
    for (auto &item : items) {
        cout << "    ";
        item.view();
    }
}

void Tdlt::view() {
    json data = parseFile();

    vector<Task> completed;
    vector<Task> uncompleted;
    vector<Task> ideas;

    for (const auto &item : data["items"]) {
        Task task = Task(item["id"], item["title"], item["progress"]);
        if (task.progress == 0.f) {
            ideas.push_back(task);
        } else if (task.progress == 1.0f) {
            completed.push_back(task);
        } else {
            uncompleted.push_back(task);
        }
    }

    viewCategory("Ideas", YELLOW, ideas);
    viewCategory("Uncompleted", RED, uncompleted);
    viewCategory("Completed", BLUE, completed);
}


